# 1° Portfolio Promo 14

Hello! This is my first portfolio for the Promo 14 that will be updated in the future.

Here you'll find my maquette and my portfolio!

My theme/logo comes from an older project I did with SAS Hackeuse and to change it a bit I decided to add the theme "hiding in plain sight"! You need to hover over the page to be able to see my portfolio!

Hope you like it!

[Here is my maquette](https://www.figma.com/file/83Ix2UeVJmcZ8lUtPdKy0r/maquette-portfolio?node-id=0%3A1)


[Here is my portfolio](https://davinadena.gitlab.io/this-one-doesnt-look-so-bad/)
